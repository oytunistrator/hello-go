package main

import (
    "fmt"
    "math"
    "os"
    "os/exec"
    "runtime"
    "time"
)

var clear map[string]func()

func init() {
    clear = make(map[string]func()) //Initialize it
    clear["darwin"] = func() {
        cmd := exec.Command("clear") //Linux example, its tested
        cmd.Stdout = os.Stdout
        cmd.Run()
    }
    clear["linux"] = func() {
        cmd := exec.Command("clear") //Linux example, its tested
        cmd.Stdout = os.Stdout
        cmd.Run()
    }
    clear["windows"] = func() {
        cmd := exec.Command("cls") //Windows example it is untested, but I think its working
        cmd.Stdout = os.Stdout
        cmd.Run()
    }
}

func CallClear() {
    value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
    if ok {                          //if we defined a clear func for that platform:
        time.Sleep(time.Second * 1)
        value() //we execute it
    } else { //unsupported platform
        panic("Your platform is unsupported! I can't clear terminal screen :(")
    }
}

// Reverse returns its argument string reversed rune-wise left to right.
func Reverse(s string) string {
    r := []rune(s)
    for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
        r[i], r[j] = r[j], r[i]
    }
    return string(r)
}

func BreakLine() {
    fmt.Printf("\n")
    time.Sleep(time.Second * 1)
}

func add(x float64, y float64) float64 {
    return x + y
}

func swap(x, y string) (string, string) {
    return y, x
}

func main() {
    pi := math.Pi
    CallClear()
    fmt.Printf(Reverse("!oG ,olleH"))
    BreakLine()
    fmt.Printf("This program is say to hello with 1 secconds late. :)")
    BreakLine()
    CallClear()
    fmt.Printf("Pi: %g.", pi)
    BreakLine()
    fmt.Printf("Pi + Pi: %g.", add(pi, pi))
    BreakLine()
    fmt.Printf("I'm your smarter AI.")
    BreakLine()
    fmt.Printf("My Name is PiFucker!")
    BreakLine()
    fmt.Printf("You fucked up! :D")
    BreakLine()
    fmt.Printf("Adding Pi + Pi with (ten * ten) * (ten * ten) [100000] times...")
    var topresult float64
    var tentensquare float64
    for i := 0; i <= 100; i++ {
        var tententen float64
        for d := 0; d <= 100; d++ {
            tententen += add(pi, pi)
        }
        tentensquare += tententen * float64(i)
        topresult += tentensquare
        BreakLine()
        fmt.Printf("Pi + Pi (with %d): %g.", i*100, tentensquare)
    }
    BreakLine()
    CallClear()
    var problemcount float64 = tentensquare + topresult*float64((100*100)+6)
    fmt.Printf("Now you have %g problems now.", problemcount)
    BreakLine()
    fmt.Printf("You successfully fucked Pi number!")
    BreakLine()
    fmt.Printf("You spend %d times on this program maybe. XD", int((100*100)+6))
}
